public class Comandes {
   
    String data_comanda;
    int preu;
    boolean pagada;



    public Comandes(String data_comanda, int preu, boolean pagada) {
        this.data_comanda = data_comanda;
        this.preu = preu;
        this.pagada = pagada;
    }

    public String getData_comanda() {
        return data_comanda;
    }

    public void setData_comanda(String data_comanda) {
        this.data_comanda = data_comanda;
    }

    public int getPreu() {
        return preu;
    }

    public void setPreu(int preu) {
        this.preu = preu;
    }

    public boolean isPagada() {
        return pagada;
    }

    public void setPagada(boolean pagada) {
        this.pagada = pagada;
    }

    @Override
    public String toString() {
        return "Comandes{" +
                "data_comanda='" + data_comanda + '\'' +
                ", preu=" + preu +
                ", pagada=" + pagada +
                '}';
    }
}

import com.mongodb.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Scanner;

public class proba {

    private ArrayList<Client> clients = new ArrayList<Client>();
    private ArrayList<Comandes> comandes = new ArrayList<Comandes>();

    private static MongoClient mongoClient;
    private static DB database;
    private static DBCollection collection;
    private static BasicDBObject dbObject;
    private static Client client = new Client();


    public static void conecting() {
        mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));
        database = mongoClient.getDB("client");
        collection = database.getCollection("client");
        new BasicDBObject("nif", client.getNif()).append("nom", client.getNom()).append("total_fact", client.getTotal_fact()).append("telefon", client.getTel()).append("correu", client.getCorreu()).append("comandes", client.getComandes());
    }


    public static void menu() {
        System.out.println("------Menu------" +
                "\n1.Donar d'alta un client" +
                "\n2.Afegir comandes a un client" +
                "\n3.Cercar clients per facturacio" +
                "\n4.Cercar clients per quantitat de comandes" +
                "\n0.Sortir");
    }


    public static void bloquejarPantalla() {
        Scanner in = new Scanner(System.in);
        System.out.print("\nToca 'C' per a continuar ");
        while (in.hasNext()) {
            if ("C".equalsIgnoreCase(in.next())) break;
        }
    }

    public static void ex1() {
        DBObject query = new BasicDBObject("nif", "12345678X").append("nom", "Pere Pons").append("total_fact", 0);
        collection.insert(query);
        System.out.println(query);


    }

    public static void altaclient() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Es demanaran les dades d'un client, siusplau introdueixles a continuacio.");

        System.out.println("Introdueix el nif");
        String nif = sc.nextLine();

        System.out.println("Introdueix el nom");
        String nom = sc.nextLine();

        System.out.println("Introdueix el total facturat");
        String total_fact = sc.nextLine();
        int total_facturacioint = Integer.parseInt(total_fact);

        System.out.println("Introdueix el telefon");
        String tel = sc.nextLine();

        System.out.println("Introdueix el correu");
        String correu = sc.nextLine();

        DBObject query = new BasicDBObject("nif", nif).append("nom", nom).append("total_fact", total_facturacioint).append("telefon", tel).append("correu", correu);
        collection.insert(query);
        System.out.println(query);
    }

    public static void ex3() {
        DBObject query = new BasicDBObject();
        DBCursor cursor = collection.find(query);
        int count = 0;
        while (cursor.hasNext()) {
            DBObject next = cursor.next();
            System.out.println("Client num: " + count + " amb Nif :" + next.get("nif"));
            count++;

        }

    }

    public static void afegircomandesclient() {

        ArrayList<String> clients = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        DBObject query = new BasicDBObject();
        DBCursor cursor = collection.find(query);
        int count = 0;

        while (cursor.hasNext()) {
            DBObject next = cursor.next();
            clients.add((String) next.get("nom"));
            System.out.println("Client num: " + count + " amb Nif :" + next.get("nif"));
            count++;
        }
        System.out.println("Escogeix el client a afegir una comanda");
        String nif = sc.nextLine();
        System.out.println("Afegeix una Comanda sencera al client amb nif " + nif);

        System.out.println("Data de la comanda");
        String data = sc.next();

        System.out.println("Afegeix l'import de la comanda");
        int preu = sc.nextInt();

        System.out.println("S'ha realitzat el pagament?(True/false)");
        Boolean bool = sc.nextBoolean();

        Comandes comandes = new Comandes(data, preu, bool);

        ArrayList<Comandes> comandesainserir = new ArrayList<Comandes>();
        comandesainserir.add(comandes);

        Client client = new Client();
        client.setComandes(comandesainserir);

  BasicDBObject queryinitial=new BasicDBObject();
  queryinitial.put("nif",nif);
        BasicDBObject wherequery=new BasicDBObject();
        wherequery.put("comandes",comandesainserir);
        BasicDBObject update=new BasicDBObject();
        update.put("$set",wherequery);
        collection.update(queryinitial,update);



    }


    public static void cercarclientfact(int fact) {
        BasicDBObject wherequery = new BasicDBObject();
        wherequery.put("total_fact", new BasicDBObject("$gt", fact));
        DBCursor cursor = collection.find(wherequery);
        while (cursor.hasNext()) {
            System.out.println(cursor.next());
        }


    }

    public static void cercarclientscomandes(int factures) {

        BasicDBObject wherequery = new BasicDBObject();
        wherequery.put("comandes", new BasicDBObject("$size", new BasicDBObject("$gt", factures)));
        DBCursor cursor = collection.find(wherequery);
        while (cursor.hasNext()) {
            System.out.println(cursor.next());
        }


    }


    public static void main(String[] args) {
        menu();
        Scanner sc = new Scanner(System.in);
        System.out.println("Opcio?");
        int opcio;
        conecting();
        do {
            opcio = sc.nextInt();

            switch (opcio) {
                case 1:
                    altaclient();
                    bloquejarPantalla();
                    menu();

                    break;
                case 2:
                    afegircomandesclient();
                    bloquejarPantalla();
                    menu();

                    break;
                case 3:
                    System.out.println("Escolleix un preu per cercar clients amb una factura mes gran");
                    int fact = sc.nextInt();
                    cercarclientfact(fact);
                    bloquejarPantalla();
                    menu();

                    break;
                case 4:
                    System.out.println("Escolleix una cantitat de comandes per cercar clients amb comandes minimes");
                    int comandes = sc.nextInt();
                    cercarclientscomandes(comandes);
                    bloquejarPantalla();
                    menu();

                    break;
                case 0:

                    break;
                default:
                    System.out.println("Opcio incorrecta\n\n");
                    menu();
            }
        } while (opcio != 0);
    }
}

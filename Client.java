import java.util.ArrayList;

public class Client {

    String nif;
    String nom;
    int total_fact;
    String tel;
    String correu;
    ArrayList<Comandes>comandes=new ArrayList<Comandes>();

    public Client() {
    }

    public Client(String nif, String nom, int total_fact, String tel, String correu, ArrayList<Comandes> comandes) {
        this.nif = nif;
        this.nom = nom;
        this.total_fact = total_fact;
        this.tel = tel;
        this.correu = correu;
        this.comandes = comandes;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getTotal_fact() {
        return total_fact;
    }

    public void setTotal_fact(int total_fact) {
        this.total_fact = total_fact;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCorreu() {
        return correu;
    }

    public void setCorreu(String correu) {
        this.correu = correu;
    }

    public ArrayList<Comandes> getComandes() {
        return comandes;
    }

    public void setComandes(ArrayList<Comandes> comandes) {
        this.comandes = comandes;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", total_fact=" + total_fact +
                ", tel='" + tel + '\'' +
                ", correu='" + correu + '\'' +
                ", comandes=" + comandes +
                '}';
    }
}
